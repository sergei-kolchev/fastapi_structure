from sqladmin.authentication import AuthenticationBackend
from starlette.requests import Request

from src.config import settings
from src.dependencies.user import get_current_user
from src.logger import logger
from src.models.user import Role
from src.repositories.user import UserRepository
from src.schemas.user import UserProfileSchema
from src.services.auth import AuthService
from src.utils.auth import create_access_token


class AdminAuth(AuthenticationBackend):
    @classmethod
    def is_admin(cls, user: UserProfileSchema | None) -> bool:
        if user and user.role == Role.ADMIN:
            return True
        return False

    async def login(self, request: Request) -> bool:
        form = await request.form()
        email, password = form["username"], form["password"]
        auth_service = AuthService(UserRepository)
        user = await auth_service.authenticate_user(email, password)  # type: ignore
        if self.is_admin(user):
            access_token = create_access_token({"sub": str(user.id)})  # type: ignore
            request.session.update({"token": access_token})
            logger.info("Administrator login", extra=dict(user))  # type: ignore
        return True

    async def logout(self, request: Request) -> bool:
        request.session.clear()
        return True

    async def authenticate(self, request: Request) -> bool:
        token = request.session.get("token")
        if not token:
            return False
        user = await get_current_user(token)
        if not (self.is_admin(user)):
            return False
        return True


authentication_backend = AdminAuth(secret_key=settings.SQLADMIN_SECRET_KEY)
