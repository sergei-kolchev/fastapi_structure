from enum import IntEnum

from sqlalchemy.orm import Mapped, mapped_column

from src.db import Base


class Role(IntEnum):
    ADMIN = 1
    USER = 2


class User(Base):
    __tablename__ = "users"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    email: Mapped[str]
    hashed_password: Mapped[str]
    role: Mapped[int] = mapped_column(default=Role.USER)

    def __str__(self):
        return f"User {self.email}"
