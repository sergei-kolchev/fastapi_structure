from typing import Type

from pydantic import TypeAdapter

from src.repositories.base import AbstractRepository
from src.schemas.user import UserSchema


class UserService:
    def __init__(self, repository: Type[AbstractRepository]):
        self._repository = repository()

    async def get_all(self) -> list[UserSchema]:
        return TypeAdapter(list[UserSchema]).validate_python(
            await self._repository.get_all()
        )
