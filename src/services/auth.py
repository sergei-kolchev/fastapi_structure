from typing import Type

from fastapi import Response
from pydantic import EmailStr

from src.exceptions import IncorrectEmailOrPasswordException, UserAlreadyExistsException
from src.repositories.base import AbstractRepository
from src.schemas.auth import UserAuthSchema
from src.schemas.user import UserProfileSchema
from src.utils.auth import create_access_token, get_password_hash, verify_password


class AuthService:
    def __init__(self, repository: Type[AbstractRepository]):
        self._repository: AbstractRepository = repository()

    async def authenticate_user(
        self, email: EmailStr, password: str
    ) -> UserProfileSchema | None:
        user = await self._repository.get_one_or_none(email=email)

        if user and verify_password(password, user.hashed_password):
            return UserProfileSchema.from_orm(user)
        return

    async def register_user(self, user_data: UserAuthSchema) -> None:
        existing_user = await self._repository.get_one_or_none(email=user_data.email)

        if existing_user:
            raise UserAlreadyExistsException

        hashed_password = get_password_hash(user_data.password)
        await self._repository.add(
            email=user_data.email, hashed_password=hashed_password
        )

    async def login(self, response: Response, user_data: UserAuthSchema) -> dict:
        user = await self.authenticate_user(user_data.email, user_data.password)

        if not user:
            raise IncorrectEmailOrPasswordException

        access_token = create_access_token({"sub": str(user.id)})
        response.set_cookie("notes_access_token", access_token, httponly=True)

        return {"access_token": access_token}

    async def logout_user(self, response: Response) -> None:
        response.delete_cookie("notes_access_token")
