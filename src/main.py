import time
from contextlib import asynccontextmanager

import uvicorn
from fastapi import FastAPI, Request
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from prometheus_fastapi_instrumentator import Instrumentator
from redis import asyncio as aioredis
from sqladmin import Admin

from src.admin.auth import authentication_backend
from src.admin.views import UserAdmin
from src.api.v1.api import api_router as api_router_v1
from src.config import settings
from src.db.init_db import engine
from src.logger import logger


@asynccontextmanager
async def lifespan(app: FastAPI):
    redis = aioredis.from_url(
        "redis://localhost:6379", encoding="utf8", decode_responses=True
    )
    FastAPICache.init(RedisBackend(redis), prefix="cache")
    yield


app = FastAPI(lifespan=lifespan)


app.include_router(api_router_v1, prefix="/api/v1")


admin = Admin(app=app, engine=engine, authentication_backend=authentication_backend)
admin.add_view(UserAdmin)


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    if settings.MODE == "DEV":
        start_time = time.time()
        response = await call_next(request)
        process_time = time.time() - start_time
        logger.info(
            "Request execution time", extra={"process_time": round(process_time, 4)}
        )
    else:
        response = await call_next(request)
    return response


instrumentator = Instrumentator(
    should_group_status_codes=False, excluded_handlers=[".*admin.*", "/metrics"]
)
instrumentator.instrument(app).expose(app)


if __name__ == "__main__":
    uvicorn.run("src.main:app", host="127.0.0.1", port=8000, reload=True)
