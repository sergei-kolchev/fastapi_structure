from src.repositories.user import UserRepository
from src.services.auth import AuthService


async def get_auth_service():
    return AuthService(UserRepository)
