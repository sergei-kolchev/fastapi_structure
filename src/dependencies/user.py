from fastapi import Depends, HTTPException
from jose import ExpiredSignatureError, JWTError, jwt
from starlette import status
from starlette.requests import Request

from src.config import settings
from src.exceptions import (
    IncorrectTokenFormatException,
    TokenAbsentException,
    TokenExpiredException,
    UserIsNotPresentException,
)
from src.models import User
from src.models.user import Role
from src.repositories.user import UserRepository
from src.schemas.user import UserProfileSchema
from src.services.user import UserService


def get_token(request: Request):
    token = request.cookies.get("notes_access_token")
    if not token:
        raise TokenAbsentException
    return token


async def get_current_user(token: str = Depends(get_token)) -> UserProfileSchema:
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, settings.ALGORITHM)
    except ExpiredSignatureError:
        raise TokenExpiredException
    except JWTError:
        raise IncorrectTokenFormatException
    user_id = payload.get("sub")
    if not user_id:
        raise UserIsNotPresentException
    user = await UserRepository().get_by_id(int(user_id))
    if not user:
        raise UserIsNotPresentException
    return user


async def get_current_admin_user(current_user: User = Depends(get_current_user)):
    if current_user.role != Role.ADMIN:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
    return current_user


async def get_user_service():
    return UserService(UserRepository)
