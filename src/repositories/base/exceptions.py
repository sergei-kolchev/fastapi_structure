class AppException(Exception):
    pass


class CannotAddDataException(AppException):
    message = "Cannot add data"


class CannotUpdateDataException(AppException):
    message = "Cannot update data"


class CannotDeleteDataException(AppException):
    message = "Cannot delete data"
