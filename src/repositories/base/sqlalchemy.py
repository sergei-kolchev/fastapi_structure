from typing import Any

from sqlalchemy import and_, delete, insert, select, update
from sqlalchemy.exc import SQLAlchemyError

from src.db import async_session_maker
from src.logger import logger
from src.repositories.base.abstract import AbstractRepository, ModelType
from src.repositories.base.exceptions import (
    CannotAddDataException,
    CannotDeleteDataException,
    CannotUpdateDataException,
)


class SQLAlchemyRepository(AbstractRepository):
    model: Any = None

    def __init__(self):
        if not self.model:
            raise AttributeError("Model can't be an empty")

    async def get_all(self, **filter_by) -> list[ModelType]:
        async with async_session_maker() as session:
            query = select(self.model).filter_by(**filter_by)
            result = await session.execute(query)
            return result.scalars().all()

    async def get_one_or_none(self, **filter_by) -> Any | None:
        async with async_session_maker() as session:
            query = select(self.model).filter_by(**filter_by)
            result = await session.execute(query)
            return result.scalar_one_or_none()

    async def get_by_id(self, id: int) -> Any | None:
        async with async_session_maker() as session:
            query = select(self.model).filter_by(id=id)
            result = await session.execute(query)
            return result.scalar_one_or_none()

    async def add(self, **data) -> Any:
        try:
            async with async_session_maker() as session:
                stmt = insert(self.model).values(**data).returning(self.model)
                result = await session.execute(stmt)
                await session.commit()
                return result.scalars().first()
        except (SQLAlchemyError, Exception) as ex:
            msg = ": Cannot add object"
            if isinstance(ex, SQLAlchemyError):
                msg = f"Database exception{msg}"
            elif isinstance(ex, Exception):
                msg = f"Unknown exception{msg}"
            logger.error(msg, extra=data, exc_info=True)
            raise CannotAddDataException

    async def update(self, id: int, **data) -> Any:
        user_id = data.get("user_id")
        try:
            async with async_session_maker() as session:
                stmt = (
                    update(self.model)
                    .where(and_(self.model.user_id == user_id, self.model.id == id))
                    .values(**data)
                    .filter_by(id=id)
                    .returning(self.model)
                )
                result = await session.execute(stmt)
                await session.commit()
                return result.scalars().first()
        except (SQLAlchemyError, Exception) as ex:
            msg = ": Cannot update object"
            if isinstance(ex, SQLAlchemyError):
                msg = f"Database exception{msg}"
            elif isinstance(ex, Exception):
                msg = f"Unknown exception{msg}"
            logger.error(msg, extra=data, exc_info=True)
            raise CannotUpdateDataException

    async def delete(self, **filter_by) -> Any:
        try:
            async with async_session_maker() as session:
                stmt = delete(self.model).filter_by(**filter_by).returning(self.model)
                result = await session.execute(stmt)
                await session.commit()
            return result.scalars().one()
        except (SQLAlchemyError, Exception) as ex:
            msg = ": Cannot delete object"
            if isinstance(ex, SQLAlchemyError):
                msg = f"Database exception{msg}"
            elif isinstance(ex, Exception):
                msg = f"Unknown exception{msg}"
            logger.error(msg, extra=filter_by, exc_info=True)
            raise CannotDeleteDataException
