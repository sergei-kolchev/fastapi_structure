from abc import ABC, abstractmethod
from typing import Any, Type

ModelType = Type[Any]


class AbstractRepository(ABC):

    @abstractmethod
    async def get_all(self, **filter_by) -> list[ModelType]:
        raise NotImplementedError

    @abstractmethod
    async def get_one_or_none(self, **filter_by) -> Any | None:
        raise NotImplementedError

    @abstractmethod
    async def get_by_id(self, id: int) -> Any | None:
        raise NotImplementedError

    @abstractmethod
    async def add(self, **data) -> Any:
        raise NotImplementedError

    @abstractmethod
    async def update(self, id: int, **data) -> Any:
        raise NotImplementedError

    @abstractmethod
    async def delete(self, **filter_by) -> Any:
        raise NotImplementedError
