from typing import Any

from sqlalchemy import and_, update
from sqlalchemy.exc import SQLAlchemyError

from src.db import async_session_maker
from src.logger import logger
from src.models import User
from src.repositories.base import SQLAlchemyRepository
from src.repositories.base.exceptions import CannotUpdateDataException


class UserRepository(SQLAlchemyRepository):
    model = User

    async def update(self, id: int, **data) -> Any:
        try:
            async with async_session_maker() as session:
                stmt = (
                    update(self.model)
                    .where(and_(self.model.id == id))
                    .values(**data)
                    .filter_by(id=id)
                    .returning(self.model)
                )
                result = await session.execute(stmt)
                await session.commit()
                return result.scalars().first()
        except (SQLAlchemyError, Exception) as ex:
            msg = ": Cannot update object"
            if isinstance(ex, SQLAlchemyError):
                msg = f"Database exception{msg}"
            elif isinstance(ex, Exception):
                msg = f"Unknown exception{msg}"
            logger.error(msg, extra=data, exc_info=True)
            raise CannotUpdateDataException
