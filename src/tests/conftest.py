import json

import pytest
from httpx import ASGITransport, AsyncClient
from sqlalchemy import insert, text

from src.config import settings
from src.db.init_db import Base, async_session_maker, engine
from src.main import app as fastapi_app
from src.models.user import User


@pytest.fixture(scope="function", autouse=True)
async def prepare_database():
    assert settings.MODE == "TEST"

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)

    def open_mock_json(model: str):
        with open(f"src/tests/mock_{model}.json", encoding="utf-8") as file:
            return json.load(file)

    users = open_mock_json("users")

    async with async_session_maker() as sess:
        add_users = insert(User).values(users)
        await sess.execute(add_users)
        await sess.commit()

        # update sequences
        query = "SELECT setval('users_id_seq', (SELECT MAX(id) from users));"
        await sess.execute(text(query))


@pytest.fixture(scope="function")
async def ac():  # ac - sync client
    async with AsyncClient(
        transport=ASGITransport(app=fastapi_app), base_url="http://test"
    ) as ac:
        yield ac


@pytest.fixture(scope="session")
async def authenticated_ac():  # ac - sync client
    async with AsyncClient(
        transport=ASGITransport(app=fastapi_app), base_url="http://test"
    ) as ac:
        await ac.post(
            "/api/v1/auth/login", json={"email": "admin@admin.com", "password": "admin"}
        )
        assert ac.cookies.get("notes_access_token")
        yield ac


@pytest.fixture(scope="function")
async def session():
    async with async_session_maker() as session:
        yield session
