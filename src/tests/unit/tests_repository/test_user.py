import pytest

from src.repositories.base.exceptions import (
    CannotAddDataException,
    CannotDeleteDataException,
    CannotUpdateDataException,
)
from src.repositories.user import UserRepository


async def test_get_all_users_ok():
    repository = UserRepository()
    users = await repository.get_all()
    assert len(users) == 2
    assert users[0].email == "test@test.com"
    assert users[0].id == 1


@pytest.mark.parametrize("id,result", [(1, 1), (2, 2), (3, None)])
async def test_get_one_or_none(id, result):
    repository = UserRepository()
    user = await repository.get_one_or_none(id=id)
    if user:
        assert user.id == result
    else:
        assert not user


async def test_get_by_id_ok():
    repository = UserRepository()
    user = await repository.get_by_id(id=1)
    assert user.id == 1
    assert user.email == "test@test.com"
    assert user.role == 2


async def test_get_by_id_none():
    repository = UserRepository()
    user = await repository.get_by_id(id=3)
    assert not user


async def test_add_ok():
    repository = UserRepository()
    user = {
        "email": "test1.@test.com",
        "hashed_password": "password_test",
    }
    added_user = await repository.add(**user)
    assert added_user.id == 3
    assert added_user.email == user.get("email")
    assert added_user.hashed_password != ""
    assert added_user.role == 2


async def test_add_user_error():
    repository = UserRepository()
    user = {
        "email": "test1.@test.com",
    }
    with pytest.raises(CannotAddDataException):
        await repository.add(**user)


async def test_update_user_ok():
    repository = UserRepository()
    user = {
        "id": 1,
        "email": "test1@test@mail.com",
    }
    updated_user = await repository.update(**user)
    assert updated_user.id == user.get("id")
    assert updated_user.email == user.get("email")


async def test_update_user_error():
    repository = UserRepository()
    user = {"id": 1, "email": "test1@test@mail.com", "wrong key": "wrong value"}
    with pytest.raises(CannotUpdateDataException):
        await repository.update(**user)


async def test_delete_user_ok():
    repository = UserRepository()
    deleted_user = await repository.delete(id=2)
    assert deleted_user.id == 2
    assert deleted_user.email == "admin@admin.com"
    assert deleted_user.role == 1


async def test_delete_user_error():
    repository = UserRepository()
    with pytest.raises(CannotDeleteDataException):
        await repository.delete(id=999)
