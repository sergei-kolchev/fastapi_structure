import pytest


@pytest.mark.parametrize(
    "email,password,status_code",
    [("user@mail.com", "user", 200), ("user1@mail.com", "user1", 200)],
)
async def test_register_user_ok(email, password, status_code, ac):
    response = await ac.post(
        "/api/v1/auth/register", json={"email": email, "password": password}
    )
    assert response.status_code == status_code


@pytest.mark.parametrize(
    "email,password,status_code",
    [
        ("user@mail.com", "user", 200),
        ("test@test.com", "user", 409),
        ("user1", "user1", 422),
    ],
)
async def test_register_user_error(email, password, status_code, ac):
    response = await ac.post(
        "/api/v1/auth/register", json={"email": email, "password": password}
    )
    assert response.status_code == status_code


@pytest.mark.parametrize(
    "email,password,status_code",
    [("test@test.com", "test", 200), ("admin@admin.com", "admin", 200)],
)
async def test_login_user_ok(email, password, status_code, ac):
    response = await ac.post(
        "/api/v1/auth/login", json={"email": email, "password": password}
    )
    assert response.status_code == status_code


@pytest.mark.parametrize(
    "email,password,status_code",
    [("error@test.com", "test", 401), ("error@admin.com", "admin", 401)],
)
async def test_login_user_error(email, password, status_code, ac):
    response = await ac.post(
        "/api/v1/auth/login", json={"email": email, "password": password}
    )
    assert response.status_code == status_code
