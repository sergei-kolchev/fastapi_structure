import os
from functools import lru_cache
from typing import Literal

from get_docker_secret import get_docker_secret
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    MODE: Literal["PROD", "DEV", "TEST"]
    LOG_LEVEL: Literal["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
    DB_HOST: str
    DB_PORT: int
    DB_USER: str
    DB_PASS: str
    DB_NAME: str
    REDIS_HOST: str
    REDIS_PORT: str
    SECRET_KEY: str
    ALGORITHM: str
    SQLADMIN_SECRET_KEY: str

    @property
    def DATABASE_URL(self):
        return (
            f"postgresql+asyncpg://{settings.DB_USER}:{settings.DB_PASS}"
            f"@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}"
        )

    model_config = SettingsConfigDict(
        env_file=(".env.dev", ".env.test"), extra="ignore"
    )


@lru_cache()
def get_settings():
    env_file = f".env.{os.environ.get('MODE', 'DEV').lower()}"
    return Settings(_env_file=env_file)


mode = os.environ.get("MODE")

if mode == "PROD":
    env_names = [
        "DB_USER",
        "DB_PASS",
        "DB_NAME",
        "SECRET_KEY",
        "ALGORITHM",
        "SQLADMIN_SECRET_KEY",
    ]

    for name in env_names:
        path = os.environ.get(name + "_FILE")
        if path:
            os.environ[name] = str(
                get_docker_secret(path.split("/")[-1], secrets_dir="/run/secrets")
            )

settings = get_settings()
