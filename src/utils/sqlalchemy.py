from src.repositories.base.abstract import ModelType


# УДАЛИТЬ!!!
def model_to_dict(model: ModelType) -> dict:
    return {c.name: str(getattr(model, c.name)) for c in model.__table__.columns}
