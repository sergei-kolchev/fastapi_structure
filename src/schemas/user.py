from pydantic import BaseModel, ConfigDict, EmailStr, SecretStr

from src.models.user import Role


class UserSchema(BaseModel):
    id: int
    email: EmailStr
    hashed_password: SecretStr
    role: Role

    model_config = ConfigDict(from_attributes=True)


class UserProfileSchema(BaseModel):
    id: int
    email: EmailStr
    role: Role

    model_config = ConfigDict(from_attributes=True)
