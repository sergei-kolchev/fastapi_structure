from fastapi import APIRouter

from src.api.v1.routers import auth, pages, user

api_router = APIRouter()

api_router.include_router(auth.router, prefix="/auth", tags=["Authentication"])
api_router.include_router(user.router, prefix="/users", tags=["Users"])

api_router.include_router(pages.router, prefix="/pages", tags=["Pages"])
