from typing import Annotated

from fastapi import APIRouter, Depends, Request
from fastapi.templating import Jinja2Templates

from src.api.v1.routers.user import profile
from src.models import User

router = APIRouter()

templates = Jinja2Templates(directory="src/templates")


@router.get("/profile")
async def get_user_profile(request: Request, user: Annotated[User, Depends(profile)]):
    return templates.TemplateResponse(
        name="profile.html", context={"request": request, "user": user}
    )
