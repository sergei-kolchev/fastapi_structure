from typing import Annotated

from fastapi import APIRouter, Depends
from starlette.responses import Response

from src.dependencies.auth import get_auth_service
from src.schemas.auth import UserAuthSchema
from src.services.auth import AuthService

router = APIRouter()


@router.post("/register")
async def register_user(
    user_data: UserAuthSchema,
    service: Annotated[AuthService, Depends(get_auth_service)],
) -> None:
    return await service.register_user(user_data)


@router.post("/login")
async def login(
    response: Response,
    user_data: UserAuthSchema,
    service: Annotated[AuthService, Depends(get_auth_service)],
) -> dict:
    return await service.login(response, user_data)


@router.post("/logout")
async def logout_user(
    response: Response, service: Annotated[AuthService, Depends(get_auth_service)]
) -> None:
    await service.logout_user(response)
