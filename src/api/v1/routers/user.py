from typing import Annotated

from fastapi import APIRouter, Depends

from src.dependencies.user import (
    get_current_admin_user,
    get_current_user,
    get_user_service,
)
from src.models import User
from src.schemas.user import UserProfileSchema, UserSchema
from src.services.user import UserService

router = APIRouter()


@router.get("/profile")
async def profile(current_user: User = Depends(get_current_user)) -> UserProfileSchema:
    return UserProfileSchema.from_orm(current_user)


@router.get("/all")
async def get_all(
    current_user: Annotated[User, Depends(get_current_admin_user)],
    service: Annotated[UserService, Depends(get_user_service)],
) -> list[UserSchema]:
    return await service.get_all()
