<p align="center">
    <img src="https://i.ibb.co/VgKwkvK/fastapi-project-structure.png" alt="fastapi-project-structure" border="0" width="600">
</p>

<p align="center">

   <img src="https://img.shields.io/badge/Python-3.12-blue" alt="Python 3.12">
   <img src="https://img.shields.io/badge/FastAPI-blue" alt="FastAPI">
   <img src="https://img.shields.io/badge/License-MIT-success" alt="License">
</p>

## About

The basic structure of the FastAPI project for quick deployment of the project

Opportunities: 
- Built-in admin panel using SQLAdmin 
- Jinja2 templates for quick creation of demo pages
- PostgreSQL database
- Redis database
- Alembic for migrations
- Monitoring with prometheus and grafana
- Pre-commit hooks with linters and formatters (flake8, black, isort, pyright, bandit)
- Configured pytest with fixtures
- Gitlab CI with docker-in-docker runner

## Installation

1. Clone the repository
2. Create docker secrets:
- algorithm
- db_name
- db_pass
- db_user
- postgres_db
- postgres_password
- postgres_user
- secret_key
- sqladmin_secret_key

3. Build with `stack.yaml`

> You need to create user with admin role

For running in dev mode:

1. Create a `.env.dev` file (example - `.env-example`)
2. Execute the command:

```commandline
uvicorn src.main:app --reload
```